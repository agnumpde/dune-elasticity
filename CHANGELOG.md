# Master (will become release 2.9)

## Deprecations and removals

- The support of `Amiramesh` is dropped in dune-grid and thus the program `linear-elasticity` is removed from CMakeLists.txt

# 2.8 Release

- Introduce class `LocalDensity` for material-specific implementations
- Introduce class `LocalIntegralEnergy` to work with the densities
- Local energies and FE assemblers use now `dune-functions` power bases instead of scalar `dune-fufem` bases; the key element is the `LocalView` which contains the information for each element
- Introduce class `LocalHyperDualStiffness` and `hyperdual` to calculate gradient and hessian using hyper-dual numbers
- The class `FEAssembler` now stores a `const` reference to the basis, rather than a value.
  This allows to use the assembler with bases that are not copyable
  (as, e.g., the `RaviartThomasBasis`, see [dune-functions issue 58](https://gitlab.dune-project.org/staging/dune-functions/-/issues/58)).

## Deprecations and removals

- Redundant implementations of `LocalEnergy` classes are now replaced by combining `LocalDensity` and `LocalIntegralEnergy`
- Local energies and FE assemblers with `dune-fufem` scalar bases are now deprecated
