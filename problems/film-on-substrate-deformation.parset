#############################################
#  Grid parameters
#############################################

structuredGrid = true
lower = 0 0 0

# whole experiment: 45 mm x 10 mm x 2 mm, scaling with 10^7 such that the thickness, which is around 100 nm, so 100x10^-9 = 10^-7 is equal to 1.
# upper = 45e4 10e4 2e4
# using only a section of the whole experiment as deformed grid to start with for dune-gfe: use much smaller dimensions!
upper = 600 200 200

elements = 15 5 5

# Number of grid levels
numLevels = 3

adaptiveRefinement = true

#############################################
#  Solver parameters
#############################################

# Number of homotopy steps for the Dirichlet boundary conditions
numHomotopySteps = 1

# Tolerance of the trust region solver
tolerance = 1e-6

# Max number of steps of the trust region solver
maxTrustRegionSteps = 500

# Initial trust-region radius
initialTrustRegionRadius = 0.1

# Number of multigrid iterations per trust-region step
numIt = 1000

# Number of presmoothing steps
nu1 = 3

# Number of postsmoothing steps
nu2 = 3

# Damping for the smoothers of the multigrid solver
damping = 1.0

# Number of coarse grid corrections
mu = 1

# Number of base solver iterations
baseIt = 100

# Tolerance of the multigrid solver
mgTolerance = 1e-7

# Tolerance of the base grid solver
baseTolerance = 1e-8

############################
#   Material parameters
############################

energy = mooneyrivlin # stvenantkirchhoff, neohooke, hencky, exphencky or mooneyrivlin

[materialParameters]

## Lame parameters for stvenantkirchhoff, E = mu(3*lambda + 2*mu)/(lambda + mu)
#mu = 2.7191e+4
#lambda = 4.4364e+4

#mooneyrivlin_a = 2.7191e+6
#mooneyrivlin_b = 2.7191e+6
#mooneyrivlin_c = 2.7191e+6

#mooneyrivlin_10 = -7.28e+5 #182 20:1
#mooneyrivlin_01 = 9.17e+5
#mooneyrivlin_20 = 1.23e+5
#mooneyrivlin_02 = 8.15e+5
#mooneyrivlin_11 = -5.14e+5

#mooneyrivlin_10 = -3.01e+6 #182 2:1
#mooneyrivlin_01 = 3.36e+6
#mooneyrivlin_20 = 5.03e+6
#mooneyrivlin_02 = 13.1e+6
#mooneyrivlin_11 = -15.2e+6

mooneyrivlin_10 = -1.67e+6 #184 2:1
mooneyrivlin_01 = 1.94e+6
mooneyrivlin_20 = 2.42e+6
mooneyrivlin_02 = 6.52e+6
mooneyrivlin_11 = -7.34e+6

mooneyrivlin_30 = 0
mooneyrivlin_21 = 0
mooneyrivlin_12 = 0
mooneyrivlin_03 = 0

# volume-preserving parameter 
mooneyrivlin_k = 90e+6

# How to choose the volume-preserving parameter?
# We need a stretch of 30%  (45e4 10e4 2e4 in x-direction, so a stretch of 45e4*0.3 = 13.5e4)
# 184 2:1, mooneyrivlin_k = 90e+6 approximately (depends also on the number of grid levels!) and mooneyrivlin_energy = square or log, neumannValues = 27e4 0 0

mooneyrivlin_energy = square # log, square or ciarlet; different ways to compute the Mooney-Rivlin-Energy

# ciarlet: Fomula from "Ciarlet: Three-Dimensional Elasticity", here no penalty term is 
# log: Generalized Rivlin model or polynomial hyperelastic model, using  0.5*mooneyrivlin_k*log(det(∇φ)) as the volume-preserving penalty term
# square: Generalized Rivlin model or polynomial hyperelastic model, using 0.5*mooneyrivlin_k*(det(∇φ)-1)² as the volume-preserving penalty term

[]

#############################################
#  Boundary values
#############################################

problem = identity-deformation

###  Python predicate specifying all Dirichlet grid vertices
# x is the vertex coordinate
dirichletVerticesPredicate = "x[0] < 0.01"

###  Python predicate specifying all neumannValues grid vertices
# x is the vertex coordinate
neumannVerticesPredicate = "x[0] > 599.99"

###  Neumann values
neumannValues = 27e4 0 0

# Initial deformation
initialDeformation = "[x[0], x[1], x[2]]"
