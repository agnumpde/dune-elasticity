class DirichletValues:
    def __init__(self, homotopyParameter):
        self.homotopyParameter = homotopyParameter

    # The identity deformation of 3d classical materials
    def dirichletValues(self, x):
        # Clamp the shape in its reference configuration
        return [x[0], x[1], x[2]]
