#ifndef DUNE_ELASTICITY_ASSEMBLERS_LOCALADOLCSTIFFNESS_HH
#define DUNE_ELASTICITY_ASSEMBLERS_LOCALADOLCSTIFFNESS_HH

#include <adolc/adouble.h>            // use of active doubles
#include <adolc/drivers/drivers.h>    // use of "Easy to Use" drivers
// gradient(.) and hessian(.)
#include <adolc/interfaces.h>    // use of "Easy to Use" drivers
#include <adolc/taping.h>             // use of taping

#include <dune/fufem/utilities/adolcnamespaceinjections.hh>

#include <dune/common/fmatrix.hh>
#include <dune/istl/matrix.hh>

#include <dune/elasticity/assemblers/localfestiffness.hh>

namespace Dune::Elasticity {

/** \brief Assembles energy gradient and Hessian with ADOL-C (automatic differentiation)
 */
template<class LocalView>
class LocalADOLCStiffness
    : public LocalFEStiffness<LocalView,double>
{
    enum {gridDim=LocalView::GridView::dimension};

public:

    // accepts ADOL_C's "adouble" only
    // useHessian2: there are different basic drivers for computing the hessian by ADOL-C, namely "hessian" and "hessian2".
    //              They yield the same results, but "hessian2" seems to be faster. The user can switch to "hessian" with
    //              useHessian2=false. See ADOL-C spec for more info about the drivers
    LocalADOLCStiffness(std::shared_ptr<Dune::Elasticity::LocalEnergy<LocalView, adouble>> energy, bool useHessian2 = true)
    : localEnergy_(energy)
    , useHessian2_(useHessian2)
    {}

    virtual ~LocalADOLCStiffness() {}

    /** \brief Compute the energy at the current configuration */
    virtual double energy (const LocalView& localView,
                           const std::vector<double>& localConfiguration) const;

    /** \brief Assemble the local gradient at the current position
    *
    *     This uses the automatic differentiation toolbox ADOL_C.
    */
    virtual void assembleGradient(const LocalView& localView,
                                  const std::vector<double>& localConfiguration,
                                  std::vector<double>& localGradient);

    /** \brief Assemble the local stiffness matrix at the current position
     *
     *     This uses the automatic differentiation toolbox ADOL_C.
     */
    virtual void assembleGradientAndHessian(const LocalView& localView,
                                            const std::vector<double>& localConfiguration,
                                            std::vector<double>& localGradient,
                                            Dune::Matrix<double>& localHessian);

    std::shared_ptr<Dune::Elasticity::LocalEnergy<LocalView, adouble>> localEnergy_;
    const bool useHessian2_;

};


template<class LocalView>
double LocalADOLCStiffness<LocalView>::
energy(const LocalView& localView,
       const std::vector<double>& localConfiguration) const
{
    int rank = Dune::MPIHelper::getCommunication().rank();
    double pureEnergy;

    std::vector<adouble> localAConfiguration(localConfiguration.size());

    trace_on(rank);

    adouble energy = 0;

    for (size_t i=0; i<localConfiguration.size(); i++)
        localAConfiguration[i] <<= localConfiguration[i];

    try {
        energy = localEnergy_->energy(localView,localAConfiguration);
    } catch (Dune::Exception &e) {
        trace_off();
        throw;
    }

    energy >>= pureEnergy;

    trace_off();

    return pureEnergy;
}



template<class LocalView>
void LocalADOLCStiffness<LocalView>::
assembleGradient(const LocalView& localView,
                 const std::vector<double>& localConfiguration,
                 std::vector<double>& localGradient
                )
{
  int rank = Dune::MPIHelper::getCommunication().rank();
  // Tape energy computation.  We may not have to do this every time, but it's comparatively cheap.
  energy(localView, localConfiguration);

  /////////////////////////////////////////////////////////////////
  // Compute the energy gradient
  /////////////////////////////////////////////////////////////////

  // Compute the actual gradient
  size_t nDoubles = localConfiguration.size();

  // Compute gradient
  localGradient.resize(nDoubles);
  gradient(rank,nDoubles,localConfiguration.data(),localGradient.data());
}


template<class LocalView>
void LocalADOLCStiffness<LocalView>::
assembleGradientAndHessian(const LocalView& localView,
                           const std::vector<double>& localConfiguration,
                           std::vector<double>& localGradient,
                           Dune::Matrix<double>& localHessian
                          )
{
    int rank = Dune::MPIHelper::getCommunication().rank();

    /////////////////////////////////////////////////////////////////
    // Compute the energy gradient
    /////////////////////////////////////////////////////////////////

    // this tapes the energy computation
    assembleGradient(localView, localConfiguration, localGradient);

    /////////////////////////////////////////////////////////////////
    // Compute Hessian
    /////////////////////////////////////////////////////////////////

    size_t nDoubles = localConfiguration.size();
    localHessian.setSize(nDoubles,nDoubles);

    // allocate raw doubles: ADOL-C requires "**double" arguments
    double* rawHessian[nDoubles];
    for(size_t i=0; i<nDoubles; i++)
    {
      // we need only the lower half of the hessian, thus allocate i+1 doubles only
      rawHessian[i] = (double*)malloc((i+1)*sizeof(double));
    }

    // ADOL-C error code
    int rc;

    // ADOL-C won't accept pointers with const values, therefore the const_cast
    if ( useHessian2_ )
      rc = hessian2(rank,nDoubles,const_cast<double*>(localConfiguration.data()),rawHessian);
    else
      rc = hessian(rank,nDoubles,const_cast<double*>(localConfiguration.data()),rawHessian);

    if (rc < 0)
      DUNE_THROW(Dune::Exception, "ADOL-C has returned with error code " << rc << "!");

    // copy the raw values to the localHessian
    for (size_t i=0; i<nDoubles; i++)
    {
      for (size_t j=0; j<i; j++)
      {
        // we can only access the lower half of rawHessian!
        localHessian[i][j] = rawHessian[i][j];
        localHessian[j][i] = rawHessian[i][j];
      }
      localHessian[i][i] = rawHessian[i][i];
    }

    // don't forget the clean everything up
    for(size_t i=0; i<nDoubles; i++)
      free(rawHessian[i]);
}

} // namespace Dune::Elasticity


/** \brief Assembles energy gradient and Hessian with ADOL-C (automatic differentiation)
 */
template<class GridView, class LocalFiniteElement, class VectorType>
class
LocalADOLCStiffness
    : public LocalFEStiffness<GridView,LocalFiniteElement,VectorType>
{
    // grid types
    typedef typename GridView::Grid::ctype DT;
    typedef typename VectorType::value_type::field_type RT;
    typedef typename GridView::template Codim<0>::Entity Entity;

    // some other sizes
    enum {gridDim=GridView::dimension};

    // Hack
    typedef std::vector<Dune::FieldVector<adouble,gridDim> > AVectorType;

public:

    //! Dimension of a tangent space
    enum { blocksize = VectorType::value_type::dimension };

    LocalADOLCStiffness(const Dune::LocalEnergy<GridView, LocalFiniteElement, AVectorType>* energy)
    : localEnergy_(energy)
    {} [[deprecated("dune-elasticity with dune-fufem bases is now deprecated. Use Elasticity::LocalADOLCStiffness with LocalView concept!")]]

    /** \brief Compute the energy at the current configuration */
    virtual RT energy (const Entity& e,
               const LocalFiniteElement& localFiniteElement,
               const VectorType& localConfiguration) const;

    /** \brief Assemble the local stiffness matrix at the current position

    This uses the automatic differentiation toolbox ADOL_C.
    */
    virtual void assembleGradientAndHessian(const Entity& e,
                         const LocalFiniteElement& localFiniteElement,
                         const VectorType& localConfiguration,
                         VectorType& localGradient);

    const Dune::LocalEnergy<GridView, LocalFiniteElement, AVectorType>* localEnergy_;

};


template <class GridView, class LocalFiniteElement, class VectorType>
typename LocalADOLCStiffness<GridView, LocalFiniteElement, VectorType>::RT
LocalADOLCStiffness<GridView, LocalFiniteElement, VectorType>::
energy(const Entity& element,
       const LocalFiniteElement& localFiniteElement,
       const VectorType& localSolution) const
{
    int rank = Dune::MPIHelper::getCommunication().rank();
    double pureEnergy;

    std::vector<Dune::FieldVector<adouble,blocksize> > localASolution(localSolution.size());

    trace_on(rank);

    adouble energy = 0;

    for (size_t i=0; i<localSolution.size(); i++)
      for (size_t j=0; j<localSolution[i].size(); j++)
        localASolution[i][j] <<= localSolution[i][j];

    energy = localEnergy_->energy(element,localFiniteElement,localASolution);

    energy >>= pureEnergy;

    trace_off();

    return pureEnergy;
}



// ///////////////////////////////////////////////////////////
//   Compute gradient and Hessian together
//   To compute the Hessian we need to compute the gradient anyway, so we may
//   as well return it.  This saves assembly time.
// ///////////////////////////////////////////////////////////
template <class GridType, class LocalFiniteElement, class VectorType>
void LocalADOLCStiffness<GridType, LocalFiniteElement, VectorType>::
assembleGradientAndHessian(const Entity& element,
                const LocalFiniteElement& localFiniteElement,
                const VectorType& localSolution,
                VectorType& localGradient)
{
    int rank = Dune::MPIHelper::getCommunication().rank();
    // Tape energy computation.  We may not have to do this every time, but it's comparatively cheap.
    energy(element, localFiniteElement, localSolution);

    /////////////////////////////////////////////////////////////////
    // Compute the energy gradient
    /////////////////////////////////////////////////////////////////

    // Compute the actual gradient
    size_t nDofs = localSolution.size();
    size_t nDoubles = nDofs*blocksize;
    std::vector<double> xp(nDoubles);
    int idx=0;
    for (size_t i=0; i<nDofs; i++)
        for (size_t j=0; j<blocksize; j++)
            xp[idx++] = localSolution[i][j];

  // Compute gradient
    std::vector<double> g(nDoubles);
    gradient(rank,nDoubles,xp.data(),g.data());                  // gradient evaluation

    // Copy into Dune type
    localGradient.resize(localSolution.size());

    idx=0;
    for (size_t i=0; i<nDofs; i++)
        for (size_t j=0; j<blocksize; j++)
            localGradient[i][j] = g[idx++];

    /////////////////////////////////////////////////////////////////
    // Compute Hessian
    /////////////////////////////////////////////////////////////////

    this->A_.setSize(nDofs,nDofs);

#ifndef ADOLC_VECTOR_MODE
    std::vector<double> v(nDoubles);
    std::vector<double> w(nDoubles);

    std::fill(v.begin(), v.end(), 0.0);

    for (size_t i=0; i<nDofs; i++)
      for (size_t ii=0; ii<blocksize; ii++)
      {
        // Evaluate Hessian in the direction of each vector of the orthonormal frame
        for (size_t k=0; k<blocksize; k++)
          v[i*blocksize + k] = (k==ii);

        int rc= 3;
        MINDEC(rc, hess_vec(rank, nDoubles, xp.data(), v.data(), w.data()));
        if (rc < 0)
          DUNE_THROW(Dune::Exception, "ADOL-C has returned with error code " << rc << "!");

        for (size_t j=0; j<nDoubles; j++)
          this->A_[i][j/blocksize][ii][j%blocksize] = w[j];

        // Make v the null vector again
        std::fill(&v[i*blocksize], &v[(i+1)*blocksize], 0.0);
      }
#else
    int n = nDoubles;
    int nDirections = nDofs * blocksize;
    double* tangent[nDoubles];
    for(size_t i=0; i<nDoubles; i++)
        tangent[i] = (double*)malloc(nDirections*sizeof(double));

    double* rawHessian[nDoubles];
    for(size_t i=0; i<nDoubles; i++)
        rawHessian[i] = (double*)malloc(nDirections*sizeof(double));

    for (int j=0; j<nDirections; j++)
    {
      for (int i=0; i<n; i++)
        tangent[i][j] = 0.0;

      for (int i=0; i<embeddedBlocksize; i++)
        tangent[(j/blocksize)*embeddedBlocksize+i][j] = orthonormalFrame[j/blocksize][j%blocksize][i];
    }

    hess_mat(rank,nDoubles,nDirections,xp.data(),tangent,rawHessian);

    // Copy Hessian into Dune data type
    for(size_t i=0; i<nDoubles; i++)
      for (size_t j=0; j<nDirections; j++)
        this->A_[j/blocksize][i/embeddedBlocksize][j%blocksize][i%embeddedBlocksize] = rawHessian[i][j];

    for(size_t i=0; i<nDoubles; i++) {
        free(rawHessian[i]);
        free(tangent[i]);
    }
#endif
}

#endif
