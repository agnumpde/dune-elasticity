#ifndef DUNE_ELASTICITY_ASSEMBLERS_LOCALENERGY_HH
#define DUNE_ELASTICITY_ASSEMBLERS_LOCALENERGY_HH

#include <vector>

namespace Dune::Elasticity {

/** \brief Base class for energies defined by integrating over one grid element */
template<class LocalView, class field_type = double>
class LocalEnergy
{

public:

  /** \brief Compute the energy at the current configuration
   *
   * \param localView Container with current element and local function basis
   * \param localConfiguration The coefficients of a FE function on the current element
   */
  virtual field_type energy (const LocalView& localView,
                             const std::vector<field_type>& localConfiguration) const = 0;

};

} // namespace Dune::Elasticity

namespace Dune {

// WARNING: This interface is deprecated and will be removed!

/** \brief Base class for energies defined by integrating over one grid element */
template<class GridView, class LocalFiniteElement, class VectorType>
class
LocalEnergy
{
    typedef typename VectorType::value_type::field_type RT;
    typedef typename GridView::template Codim<0>::Entity Element;

public:

  /** \brief Compute the energy
   *
   * \param element A grid element
   * \param LocalFiniteElement A finite element on the reference element
   * \param localConfiguration The coefficients of a FE function on the current element
   */
  /** \brief Compute the energy at the current configuration */
  virtual RT energy (const Element& element,
                     const LocalFiniteElement& localFiniteElement,
                     const VectorType& localConfiguration) const = 0;

};

}  // namespace Dune

#endif  // DUNE_ELASTICITY_ASSEMBLERS_LOCALENERGY_HH

