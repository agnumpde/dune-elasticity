#include <dune/common/bitsetvector.hh>
#include <dune/common/timer.hh>

#include <dune/istl/io.hh>
#include <dune/istl/scaledidmatrix.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>

#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/fufem/functionspacebases/p1nodalbasis.hh>
#include <dune/fufem/assemblers/operatorassembler.hh>
#include <dune/fufem/assemblers/dunefunctionsoperatorassembler.hh>
#include <dune/fufem/assemblers/localassemblers/laplaceassembler.hh>
#include <dune/fufem/assemblers/localassemblers/massassembler.hh>
#include <dune/fufem/assemblers/basisinterpolationmatrixassembler.hh>
#include <dune/fufem/assemblers/istlbackend.hh>

// Using a monotone multigrid as the inner solver
#include <dune/solvers/iterationsteps/trustregiongsstep.hh>
#include <dune/solvers/iterationsteps/mmgstep.hh>
#include <dune/solvers/transferoperators/truncatedcompressedmgtransfer.hh>
#include <dune/solvers/transferoperators/mandelobsrestrictor.hh>
#include <dune/solvers/solvers/iterativesolver.hh>
#include <dune/solvers/norms/twonorm.hh>
#include <dune/solvers/norms/h1seminorm.hh>

#include <dune/elasticity/common/maxnormtrustregion.hh>


template <class BasisType, class VectorType>
void TrustRegionSolver<BasisType,VectorType>::
setup(const typename BasisType::GridView::Grid& grid,
         std::conditional_t< // do we have a dune-functions basis? -> choose right assembler type
           Dune::models<Dune::Functions::Concept::GlobalBasis<GridView>, BasisType>(),
             const Dune::Elasticity::FEAssembler<BasisType,VectorType>*,
             const Dune::FEAssembler<DuneFunctionsBasis<BasisType>,VectorType>* > assembler,
         const SolutionType& x,
         const Dune::BitSetVector<blocksize>& dirichletNodes,
         double tolerance,
         int maxTrustRegionSteps,
         double initialTrustRegionRadius,
         int multigridIterations,
         double mgTolerance,
         int mu,
         int nu1,
         int nu2,
         int baseIterations,
         double baseTolerance,
         double damping)

{
    grid_                     = &grid;
    assembler_                = assembler;
    x_                        = x;
    this->tolerance_          = tolerance;
    maxTrustRegionSteps_      = maxTrustRegionSteps;
    initialTrustRegionRadius_ = initialTrustRegionRadius;
    innerIterations_          = multigridIterations;
    innerTolerance_           = mgTolerance;
    ignoreNodes_              = std::make_shared<Dune::BitSetVector<blocksize>>(dirichletNodes);
    baseIterations_           = baseIterations;
    baseTolerance_            = baseTolerance;
    damping_                  = damping;

    const auto dim = VectorType::value_type::dimension;

#if HAVE_DUNE_PARMG
    Dune::Timer setupTimer;
    mgSetup_ = std::make_unique<MGSetup>(grid);
    mgSetup_->overlap(false);
    mgSetup_->ignore(std::const_pointer_cast<Dune::BitSetVector<blocksize> >(ignoreNodes_));

    BasisType feBasis(grid.levelGridView(grid.maxLevel()));

    std::conditional_t< // do we have a dune-functions basis? -> choose right assembler type
      Dune::models<Dune::Functions::Concept::GlobalBasis<GridView>, BasisType>(),
        BasisType,
        DuneFunctionsBasis<BasisType> > basis(feBasis);

    innerMultigridStep_ = std::make_unique<Dune::ParMG::Multigrid<VectorType> >();
    innerMultigridStep_->mu_ = mu;
    innerMultigridStep_->preSmootherSteps_ = nu1;
    innerMultigridStep_->postSmootherSteps_ = nu2;

    if (grid.comm().rank()==0)
        std::cout << "Parallel multigrid setup took " << setupTimer.stop() << " seconds." << std::endl;
#else

    std::conditional_t< // do we have a dune-functions basis?
      Dune::models<Dune::Functions::Concept::GlobalBasis<GridView>, BasisType>(),
        BasisType,
        DuneFunctionsBasis<BasisType> > basis(assembler_->basis_);
    // ////////////////////////////////
    //   Create a multigrid solver
    // ////////////////////////////////

#ifdef HAVE_IPOPT
    // First create an IPOpt base solver
    auto baseSolver = std::make_shared<QuadraticIPOptSolver<MatrixType,CorrectionType>>(
        baseTolerance,
        baseIterations,
        NumProc::QUIET,
        "mumps");
#else
    // First create a Gauss-seidel base solver
    TrustRegionGSStep<MatrixType, CorrectionType>* baseSolverStep = new TrustRegionGSStep<MatrixType, CorrectionType>;

    // Hack: the two-norm may not scale all that well, but it is fast!
    TwoNorm<CorrectionType>* baseNorm = new TwoNorm<CorrectionType>;

    auto baseSolver = std::make_shared<::LoopSolver<CorrectionType>>(baseSolverStep,
                                                                            baseIterations,
                                                                            baseTolerance,
                                                                            baseNorm,
                                                                            Solver::QUIET);
#endif
    // Make pre and postsmoothers
    auto presmoother  = std::make_shared< TrustRegionGSStep<MatrixType, CorrectionType> >();
    auto postsmoother = std::make_shared< TrustRegionGSStep<MatrixType, CorrectionType> >();

    auto mmgStep = std::make_shared< MonotoneMGStep<MatrixType, CorrectionType> >();

    mmgStep->setMGType(mu, nu1, nu2);
    mmgStep->ignoreNodes_ = &dirichletNodes;
    mmgStep->setBaseSolver(baseSolver);
    mmgStep->setSmoother(presmoother, postsmoother);
    mmgStep->setObstacleRestrictor(std::make_shared<MandelObstacleRestrictor<CorrectionType>>());
#endif

    // //////////////////////////////////////////////////////////////////////////////////////
    //   Assemble a Laplace matrix to create a norm that's equivalent to the H1-norm
    // //////////////////////////////////////////////////////////////////////////////////////


    using ScalarMatrixType = Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> >;
    ScalarMatrixType localA;

    std::conditional_t< // do we have a dune-functions basis?
      Dune::models<Dune::Functions::Concept::GlobalBasis<GridView>, BasisType>(),
        Dune::Fufem::DuneFunctionsOperatorAssembler<BasisType,BasisType>,
        OperatorAssembler<DuneFunctionsBasis<BasisType>,DuneFunctionsBasis<BasisType>,Dune::Partitions::Interior> > operatorAssembler(basis,basis);

    if constexpr ( Dune::models<Dune::Functions::Concept::GlobalBasis<GridView>, BasisType>() )
    {
      using FiniteElement = std::decay_t<decltype(basis.localView().tree().child(0).finiteElement())>;
      // construct a Fufem Basis Assembler
      auto laplaceStiffness = LaplaceAssembler<GridType, FiniteElement, FiniteElement, Dune::ScaledIdentityMatrix<double,dim>>();
      // transform it to a dune-functions assembler
      auto localAssembler = [&](const auto& element, auto& localMatrix, auto&& trialLocalView, auto&& ansatzLocalView){
        // the fufem basis assembler assumes a scalar basis but a blocked element matrix!
        // we can use ScaledIdentityMatrix here
        Dune::Matrix<Dune::ScaledIdentityMatrix<double,dim>> localBlockedMatrix( localMatrix.N()/dim, localMatrix.M()/dim );
        laplaceStiffness.assemble(element,
                                  localBlockedMatrix,
                                  trialLocalView.tree().child(0).finiteElement(),
                                  ansatzLocalView.tree().child(0).finiteElement());
        // convert back to flat matrix (we need only the diagonal entries in the blocks)
        localMatrix = 0;
        for(size_t i=0; i<localBlockedMatrix.N(); i++)
          for(size_t j=0; j<localBlockedMatrix.M(); j++)
            Dune::MatrixVector::addToDiagonal(localMatrix[i*dim][j*dim], localBlockedMatrix[i][j].scalar());
      };


      auto matrixBackend = Dune::Fufem::istlMatrixBackend(localA);
      auto patternBuilder = matrixBackend.patternBuilder();

      operatorAssembler.assembleBulkPattern(patternBuilder);
      patternBuilder.setupMatrix();

      operatorAssembler.assembleBulkEntries(matrixBackend, localAssembler);
    }
    else
    {
      LaplaceAssembler<GridType,
                      typename DuneFunctionsBasis<BasisType>::LocalFiniteElement,
                      typename DuneFunctionsBasis<BasisType>::LocalFiniteElement> laplaceStiffness;

      operatorAssembler.assemble(laplaceStiffness, localA);
    }

    ScalarMatrixType* A = new ScalarMatrixType(localA);

    h1SemiNorm_ = std::make_shared< H1SemiNorm<CorrectionType> >(*A);

    // //////////////////////////////////////////////////////////////////////////////////////
    //   Assemble a mass matrix to create a norm that's equivalent to the L2-norm
    //   This will be used to monitor the gradient
    // //////////////////////////////////////////////////////////////////////////////////////

    ScalarMatrixType localMassMatrix;

    if constexpr ( Dune::models<Dune::Functions::Concept::GlobalBasis<GridView>, BasisType>() )
    {
      // construct a Fufem Basis Assembler
      using FiniteElement = std::decay_t<decltype(basis.localView().tree().child(0).finiteElement())>;
      auto massStiffness = MassAssembler<GridType, FiniteElement, FiniteElement, Dune::ScaledIdentityMatrix<double,dim>>();
      // transform it to a dune-functions assembler
      auto localMassAssembler = [&](const auto& element, auto& localMatrix, auto&& trialLocalView, auto&& ansatzLocalView){
        // the fufem basis assembler assumes a scalar basis but a blocked element matrix!
        // we can use ScaledIdentityMatrix here
        Dune::Matrix<Dune::ScaledIdentityMatrix<double,dim>> localBlockedMatrix( localMatrix.N()/dim, localMatrix.M()/dim );
        massStiffness.assemble(element,
                               localBlockedMatrix,
                               trialLocalView.tree().child(0).finiteElement(),
                               ansatzLocalView.tree().child(0).finiteElement());
        // convert back to flat matrix (we need only the diagonal entries in the blocks)
        localMatrix = 0;
        for(size_t i=0; i<localBlockedMatrix.N(); i++)
          for(size_t j=0; j<localBlockedMatrix.M(); j++)
            Dune::MatrixVector::addToDiagonal(localMatrix[i*dim][j*dim], localBlockedMatrix[i][j].scalar());
      };

      auto massMatrixBackend = Dune::Fufem::istlMatrixBackend(localMassMatrix);
      auto massPatternBuilder = massMatrixBackend.patternBuilder();

      operatorAssembler.assembleBulkPattern(massPatternBuilder);
      massPatternBuilder.setupMatrix();

      operatorAssembler.assembleBulkEntries(massMatrixBackend, localMassAssembler);
    }
    else
    {
      MassAssembler<GridType,
                    typename DuneFunctionsBasis<BasisType>::LocalFiniteElement,
                    typename DuneFunctionsBasis<BasisType>::LocalFiniteElement> massStiffness;

      operatorAssembler.assemble(massStiffness, localMassMatrix);
    }

    ScalarMatrixType* massMatrix = new ScalarMatrixType(localMassMatrix);
    l2Norm_ = std::make_shared<H1SemiNorm<CorrectionType> >(*massMatrix);

    // ////////////////////////////////////////////////////////////
    //    Create Hessian matrix and its occupation structure
    // ////////////////////////////////////////////////////////////

    hessianMatrix_ = std::make_shared<MatrixType>();
    if constexpr ( Dune::models<Dune::Functions::Concept::GlobalBasis<GridView>, BasisType>() )
    {
      auto hessianBackend = Dune::Fufem::istlMatrixBackend(*hessianMatrix_);
      auto hessianPatternBuilder = hessianBackend.patternBuilder();
      operatorAssembler.assembleBulkPattern(hessianPatternBuilder);
      hessianPatternBuilder.setupMatrix();
    }
    else
    {
      Dune::MatrixIndexSet indices(grid_->size(1), grid_->size(1));
      assembler_->getNeighborsPerVertex(indices);
      indices.exportIdx(*hessianMatrix_);
    }
#if !HAVE_DUNE_PARMG
    innerSolver_ = std::make_shared<LoopSolver<CorrectionType> >(mmgStep,
                                                                   innerIterations_,
                                                                   innerTolerance_,
                                                                   h1SemiNorm_,
                                                                 Solver::REDUCED);
    // ////////////////////////////////////
    //   Create the transfer operators
    // ////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////
    //  The P1 space (actually P1/Q1, depending on the grid) is special:
    //  If we work in such a space, then the multigrid hierarchy of spaces
    //  is constructed in the usual way.  For all other space, there is
    //  an additional restriction operator on the top of the hierarchy, which
    //  restricts the FE space to the P1/Q1 space on the same grid.
    //  On the lower grid levels a hierarchy of P1/Q1 spaces is used again.
    ////////////////////////////////////////////////////////////////////////

    using Basis = BasisType;
    bool isP1Basis;

    if constexpr ( Dune::models<Dune::Functions::Concept::GlobalBasis<GridView>, BasisType>() )
    {
      using namespace Dune::Functions::BasisFactory;
      auto p1PowerBasis = makeBasis(
          basis.gridView(),
          power<dim>(
          lagrange<1>()
      ));

      using P1PowerBasis = decltype(p1PowerBasis);
      isP1Basis = std::is_same<Basis,P1PowerBasis>::value;
    }
    else
    {
      isP1Basis = std::is_same<Basis,Dune::Functions::LagrangeBasis<typename Basis::GridView, 1> >::value;
    }

    int numLevels = grid_->maxLevel()+1;

    using TransferOperatorType = typename TruncatedCompressedMGTransfer<CorrectionType>::TransferOperatorType;
    std::vector<std::shared_ptr<TruncatedCompressedMGTransfer<CorrectionType>>> transferOperators(isP1Basis ? numLevels-1 : numLevels);

    // Here we set up the restriction of the leaf grid space into the leaf grid P1/Q1 space
    // TODO: IIRC that move to P1Basis was only implemented because I didn't have general
    // prolongation operators between arbitratry FE spaces.
    if (not isP1Basis)
    {
      TransferOperatorType pkToP1TransferMatrix;

      if constexpr ( Dune::models<Dune::Functions::Concept::GlobalBasis<GridView>, BasisType>() )
      {
        using namespace Dune::Functions::BasisFactory;
        auto p1PowerBasis = makeBasis(
            basis.gridView(),
            power<dim>(
            lagrange<1>()
        ));
        assembleGlobalBasisTransferMatrix(pkToP1TransferMatrix,p1PowerBasis,basis);
      }
      else
      {
        P1NodalBasis<typename GridType::LeafGridView,double> p1Basis(grid_->leafGridView());
        assembleBasisInterpolationMatrix<TransferOperatorType,
                                        P1NodalBasis<typename GridType::LeafGridView,double>,
                                        DuneFunctionsBasis<BasisType> >(pkToP1TransferMatrix,p1Basis,basis);
      }

      auto pkToP1 = std::make_shared< TruncatedCompressedMGTransfer<CorrectionType> >();
      transferOperators.back() = pkToP1;
      std::shared_ptr<TransferOperatorType> topTransferOperator = std::make_shared<TransferOperatorType>(pkToP1TransferMatrix);
      pkToP1->setMatrix(topTransferOperator);
    }

    // Now the P1/Q1 restriction operators for the remaining levels
    for (int i=0; i<numLevels-1; i++) {

        // Construct the local multigrid transfer matrix
        transferOperators[i] = std::make_shared<TruncatedCompressedMGTransfer<CorrectionType> >();
        transferOperators[i]->setup(*grid_,i,i+1);
    }

    mmgStep->setTransferOperators(transferOperators);

    // //////////////////////////////////////////////////////////
    //   Create obstacles
    // //////////////////////////////////////////////////////////

    hasObstacle_.resize(basis.size(), true);
    mmgStep->setHasObstacles(hasObstacle_);

#endif
}


template <class BasisType, class VectorType>
void TrustRegionSolver<BasisType,VectorType>::solve()
{
    int rank = grid_->comm().rank();
#if !HAVE_DUNE_PARMG
    MonotoneMGStep<MatrixType,CorrectionType>* mgStep = nullptr;

    // if the inner solver is a monotone multigrid set up a max-norm trust-region
    if (std::dynamic_pointer_cast<LoopSolver<CorrectionType>>(innerSolver_)) {
        auto loopSolver = std::dynamic_pointer_cast<LoopSolver<CorrectionType>>(innerSolver_);
        auto& iterationStep = loopSolver->getIterationStep();
        mgStep = dynamic_cast<MonotoneMGStep<MatrixType,CorrectionType>*>(&iterationStep);

    }
#else
    BasisType coarseBasis(grid_->levelGridView(0));
    std::vector<BoxConstraint<typename VectorType::field_type, blocksize>> coarseTrustRegionObstacles(coarseBasis.size());
#endif
    MaxNormTrustRegion<blocksize> trustRegion(assembler_->basis_.size(), initialTrustRegionRadius_);

    std::vector<BoxConstraint<field_type,blocksize> > trustRegionObstacles;

    // /////////////////////////////////////////////////////
    //   Trust-Region Solver
    // /////////////////////////////////////////////////////

    double oldEnergy = assembler_->computeEnergy(x_);
    oldEnergy = grid_->comm().sum(oldEnergy);

    bool recomputeGradientHessian = true;
    CorrectionType rhs;
    MatrixType stiffnessMatrix;

    Dune::Timer problemTimer;

    for (int i=0; i<maxTrustRegionSteps_; i++) {

        Dune::Timer totalTimer;
        if (this->verbosity_ == Solver::FULL and rank==0) {
            std::cout << "----------------------------------------------------" << std::endl;
            std::cout << "      Trust-Region Step Number: " << i
                      << ",     radius: " << trustRegion.radius()
                      << ",     energy: " << oldEnergy << std::endl;
            std::cout << "----------------------------------------------------" << std::endl;
        }

        Dune::Timer gradientTimer;

        if (recomputeGradientHessian) {

            assembler_->assembleGradientAndHessian(x_,
                                                   rhs,
                                                   *hessianMatrix_,
                                                   i==0    // assemble occupation pattern only for the first call
                                                   );
#if HAVE_DUNE_PARMG
            std::function<void(VectorType&)> accumulate = Dune::ParMG::makeAccumulate<VectorType>(*(mgSetup_->comms_.back()));
            accumulate(rhs);
#endif

            rhs *= -1;        // The right hand side is the _negative_ gradient

            // Compute gradient norm to monitor convergence
            CorrectionType gradient = rhs;
            for (size_t j=0; j<gradient.size(); j++)
              for (size_t k=0; k<gradient[j].size(); k++)
                if ((*ignoreNodes_)[j][k])
                  gradient[j][k] = 0;

            auto gradientNorm = l2Norm_->operator()(gradient);
            auto gradientNormSquared = gradientNorm*gradientNorm;
            auto totalGradientNorm = std::sqrt(grid_->comm().sum(gradientNormSquared));

            if (this->verbosity_ == Solver::FULL and rank==0)
              std::cout << "Gradient norm: " << totalGradientNorm << std::endl;
            if (this->verbosity_ == Solver::FULL)
              std::cout << "Assembly took " << gradientTimer.elapsed() << " sec." << std::endl;

#if HAVE_DUNE_PARMG
            if (!mgSetup_->overlap())
                Dune::ParMG::collectDiagonal(*hessianMatrix_, *mgSetup_->comms_.back());
            mgSetup_->matrix(hessianMatrix_);
#endif
            // Transfer matrix data
            stiffnessMatrix = *hessianMatrix_;

            recomputeGradientHessian = false;

        }

        CorrectionType corr(rhs.size());
        corr = 0;
        bool solvedByInnerSolver = true;

        //Take the obstacles on the finest grid and give them to the multigrid solver, it will create a hierarchy for all coarser grids
        trustRegionObstacles = trustRegion.obstacles();

#if ! HAVE_DUNE_PARMG
        mgStep->setProblem(stiffnessMatrix, corr, rhs);

        mgStep->setObstacles(trustRegionObstacles);
#else
        mgSetup_->setupObstacles(std::make_shared< std::vector<BoxConstraint<typename VectorType::field_type, blocksize>> >(trustRegionObstacles));
        mgSetup_->setupSmoother(damping_);

        auto& levelOp = mgSetup_->levelOps_;

        bool enableCoarseCorrection = true;
        if (enableCoarseCorrection)
          mgSetup_->setupCoarseIPOPTSolver(baseTolerance_, baseIterations_);
        else
          mgSetup_->setupCoarseNullSolver();

        innerMultigridStep_->levelOperations(levelOp);
        innerMultigridStep_->coarseSolver(mgSetup_->coarseSolver());

        using ProjGS = Dune::ParMG::ParallelProjectedGS<MatrixType, VectorType>;
        using namespace Dune::ParMG;


        ProjGS projGs(&stiffnessMatrix, &trustRegionObstacles, mgSetup_->ignores_.back().get());
        projGs.accumulate([&](VectorType& x) {
          levelOp.back().maybeCopyFromMaster(x);
        });
        projGs.dampening(1.0);

        std::function<void(VectorType&)> restrictToMaster = [op=levelOp.back()](VectorType& x) { op.maybeRestrictToMaster(x); };
        restrictToMaster(rhs);

        //Distributed energy norm
        auto energyNorm = Dune::ParMG::parallelEnergyNorm<VectorType>(stiffnessMatrix, restrictToMaster, grid_->comm());
        auto parallelInfinityNorm = [&](const VectorType& x) {
          double y = x.infinity_norm();
          double norm = grid_->comm().max(y);
          return norm;
        };

        levelOp.back().maybeCopyFromMaster(corr);

        VectorType b;
        unsigned step = 0;
        MPI_Barrier(grid_->comm());
        auto realIterationStep = [&](VectorType& innerIterate) {
          // Create vector b to keep rhs untouched!
          b = rhs;

          // The variable innerIterate is the correction that will be added
          // to the current iterate in the trust region step.
          innerMultigridStep_->apply(innerIterate, b);

          ++step;
        };

        auto solverNorm = std::make_shared< NormAdapter<VectorType> >(energyNorm);
        auto iterationStep = std::make_shared< LambdaStep<VectorType> >(realIterationStep, corr);
        auto innerSolver_ = std::make_shared< LoopSolver<CorrectionType> >(iterationStep,
                                                                           innerIterations_,
                                                                           innerTolerance_,
                                                                           // h1SemiNorm_, //TODO: test this!
                                                                           solverNorm,
                                                                           NumProc::QUIET);
#endif

        innerSolver_->preprocess();

        ///////////////////////////////
        //    Solve !
        ///////////////////////////////

        std::cout << "Solve quadratic problem..." << std::endl;

        Dune::Timer solutionTimer;

        try {
            innerSolver_->solve();
        } catch (Dune::Exception &e) {
            std::cerr << "Error while solving: " << e << std::endl;
            solvedByInnerSolver = false;
            corr = 0;
        }
        double energy = 0;
        double totalModelDecrease = 0;
        SolutionType newIterate = x_;

        if (solvedByInnerSolver) {

            std::cout << "Solving the quadratic problem took " << solutionTimer.elapsed() << " seconds." << std::endl;

    #if ! HAVE_DUNE_PARMG
            if (mgStep)
                corr = mgStep->getSol();

            auto correctionInfinityNorm = corr.infinity_norm();
    #else
            corr = iterationStep->getSol();

            auto correctionInfinityNorm = parallelInfinityNorm(corr);
    #endif

            //std::cout << "Correction: " << std::endl << corr << std::endl;

            if (this->verbosity_ == NumProc::FULL)
                std::cout << "Infinity norm of the correction: " << correctionInfinityNorm << std::endl;

            if (correctionInfinityNorm < this->tolerance_) {
                if (this->verbosity_ == NumProc::FULL and rank==0)
                {
                    if (correctionInfinityNorm < trustRegion.radius())
                        std::cout << "CORRECTION IS SMALL ENOUGH" << std::endl;
                    else
                        std::cout << "TRUST-REGION UNDERFLOW!" << std::endl;
                }

                if (this->verbosity_ != NumProc::QUIET and rank==0)
                    std::cout << i+1 << " trust-region steps were taken." << std::endl;
                break;
            }

            // ////////////////////////////////////////////////////
            //   Check whether trust-region step can be accepted
            // ////////////////////////////////////////////////////

            for (size_t j=0; j<newIterate.size(); j++)
                newIterate[j] += corr[j];

            try {
                energy  = assembler_->computeEnergy(newIterate);
            } catch (Dune::Exception &e) {
                std::cerr << "Error while computing the energy of the new Iterate: " << e << std::endl;
                std::cerr << "Redoing trust region step with smaller radius..." << std::endl;
                newIterate = x_;
                solvedByInnerSolver = false;
                energy = oldEnergy;
            }
            if (solvedByInnerSolver) {

                energy = grid_->comm().sum(energy);

                // compute the model decrease
                // It is $ m(x) - m(x+s) = -<g,s> - 0.5 <s, Hs>
                // Note that rhs = -g
                CorrectionType tmp(corr.size());
                tmp = 0;
                hessianMatrix_->umv(corr, tmp);
                double modelDecrease = (rhs*corr) - 0.5 * (corr*tmp);
                totalModelDecrease = grid_->comm().sum(modelDecrease);

                assert(totalModelDecrease >= 0);

                double relativeModelDecrease = totalModelDecrease / std::fabs(energy);
                double relativeFunctionalDecrease = (oldEnergy - energy)/std::fabs(energy);

                if (this->verbosity_ == NumProc::FULL and rank==0) {
                    std::cout << "Absolute model decrease: " << totalModelDecrease << std::endl;
                    std::cout << "Functional decrease: " << oldEnergy - energy << std::endl;
                    std::cout << "oldEnergy = " << oldEnergy << " and new energy = " << energy << std::endl;
                    std::cout << "Relative model decrease: " << relativeModelDecrease << std::endl;
                    std::cout << "Relative functional decrease: " << relativeFunctionalDecrease << std::endl;
                }


                if (energy >= oldEnergy) {
                    if (this->verbosity_ == NumProc::FULL and rank==0)
                        printf("Richtung ist keine Abstiegsrichtung!\n");
                }

                if (energy >= oldEnergy && (std::fabs(relativeFunctionalDecrease) < 1e-9 || std::fabs(relativeModelDecrease) < 1e-9)) {
                    if (this->verbosity_ == NumProc::FULL and rank==0)
                        std::cout << "Suspecting rounding problems" << std::endl;

                    if (this->verbosity_ != NumProc::QUIET and rank==0)
                        std::cout << i+1 << " trust-region steps were taken." << std::endl;

                    x_ = newIterate;
                    break;
                }
            }
        }

        // //////////////////////////////////////////////
        //   Check for acceptance of the step
        // //////////////////////////////////////////////
        if (solvedByInnerSolver && energy < oldEnergy && (oldEnergy-energy) / std::fabs(totalModelDecrease) > 0.9) {
            // very successful iteration

            x_ = newIterate;
            trustRegion.scale(2);

            // current energy becomes 'oldEnergy' for the next iteration
            oldEnergy = energy;

            recomputeGradientHessian = true;

        } else if (solvedByInnerSolver && ((oldEnergy-energy) / std::fabs(totalModelDecrease) > 0.01 || std::fabs(oldEnergy-energy) < 1e-12)) {
            // successful iteration
            x_ = newIterate;

            // current energy becomes 'oldEnergy' for the next iteration
            oldEnergy = energy;

            recomputeGradientHessian = true;

        } else {

            // unsuccessful iteration

            // Decrease the trust-region radius
            trustRegion.scale(0.5);

            if (this->verbosity_ == NumProc::FULL and rank==0)
                std::cout << "Unsuccessful iteration!" << std::endl;
        }

        std::cout << "iteration took " << totalTimer.elapsed() << " sec." << std::endl;
    }
    if (rank==0)
        std::cout << "The whole trust-region step took " << problemTimer.elapsed() << " sec." << std::endl;

}
