#include <dune/fufem/assemblers/functionalassembler.hh>
#include <dune/fufem/assemblers/operatorassembler.hh>

#include <dune/fufem/functions/basisgridfunction.hh>


template <class VectorType, class MatrixType, class Basis>
void NonlinearElasticityProblem<VectorType, MatrixType, Basis>::assembleQP(const VectorType& iterate)
{

    const Basis& basis = material_->basis();

    GridFunctionPtr displace = std::make_shared<BasisGridFunction<Basis,VectorType> >(basis,iterate);

    // assemble quadratic term
    OperatorAssembler<Basis,Basis> globalAssembler(basis,basis);
    globalAssembler.assemble(material_->secondDerivative(displace), A_);

    // assemble linear term
    FunctionalAssembler<Basis> funcAssembler(basis);
    funcAssembler.assemble(material_->firstDerivative(displace), f_);
    f_ -= *extForces_;
    f_ *= -1;
}

template <class VectorType, class MatrixType, class Basis>
template <class DiagonalMatrixType>
void NonlinearElasticityProblem<VectorType,MatrixType, Basis>::assembleDefectQP(
                          const VectorType& iterate,
                          VectorType& residual,
                          DiagonalMatrixType& hessian)
{

    const Basis& basis = material_->basis();

    GridFunctionPtr displace = std::make_shared<BasisGridFunction<Basis,VectorType> >(basis,iterate);

    // assemble quadratic term
    hessian = DiagonalMatrixType(basis.size());
    hessian = 0;


    const auto& locHessianAssembler =  material_->secondDerivative(displace);

    // ////////////////////////////////////////////////////////////////////////
    //   Loop over all elements and assemble diagonal entries of the hessian
    // ////////////////////////////////////////////////////////////////////////

    const typename Basis::GridView& gridView = basis.getGridView();
    typename Basis::GridView::template Codim<0>::Iterator eIt    = gridView.template begin<0>();
    typename Basis::GridView::template Codim<0>::Iterator eEndIt = gridView.template end<0>();

    for (; eIt!=eEndIt; ++eIt) {

        // Get set of shape functions
        const auto& lfe = basis.getLocalFiniteElement(*eIt);
        const auto& localBasis = lfe.localBasis();
        typename MaterialType::LocalHessian::LocalMatrix localMatrix(localBasis.size(),localBasis.size());
        locHessianAssembler.assemble(*eIt,localMatrix, lfe, lfe);

        // Add to residual vector
        for (size_t j=0; j<localBasis.size(); j++) {

            int globalRow = basis.index(*eIt, j);

            // Assemble diagonal part of second order stiffness matrix
            hessian[globalRow][globalRow] += localMatrix[j][j];

        }
    }

    // assemble linear term
    FunctionalAssembler<Basis> funcAssembler(basis);
    funcAssembler.assemble(material_->firstDerivative(displace),residual);
    residual -= *extForces_;
    residual *= -1;
}

template <class VectorType, class MatrixType, class Basis>
typename VectorType::field_type NonlinearElasticityProblem<VectorType,MatrixType, Basis>::energy(const VectorType& iterate) const
{
    // The energy is given by the material energy and the external forces
    field_type energy(0);

    // potential energy
    GridFunctionPtr displace = std::make_shared<BasisGridFunction<Basis,VectorType> >(material_->basis(),iterate);
    energy += material_->energy(displace);

    // external terms
    energy -= (*extForces_)*iterate;

    return energy;
}

template <class VectorType, class MatrixType, class Basis>
typename VectorType::field_type NonlinearElasticityProblem<VectorType,MatrixType, Basis>::modelDecrease(const VectorType& correction) const
{
    // the model decrease is simply -<f,corr> - 0.5 <corr, H corr>
    VectorType tmp(correction.size());
    A_.mv(correction, tmp);

    return (f_*correction) - 0.5 * (correction*tmp);
}



