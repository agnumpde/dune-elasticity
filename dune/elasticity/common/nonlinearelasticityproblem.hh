// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_ELASTICITY_COMMON_NONLINEAR_ELASTICITY_PROBLEM_HH
#define DUNE_ELASTICITY_COMMON_NONLINEAR_ELASTICITY_PROBLEM_HH

#include <dune/fufem/functions/basisgridfunction.hh>
#include <dune/elasticity/materials/material.hh>

template <class VectorTypeTEMPLATE, class MatrixTypeTEMPLATE, class Basis>
class NonlinearElasticityProblem
{
public:
    typedef Material<Basis> MaterialType;
    typedef typename MaterialType::GridType GridType;
    typedef VectorTypeTEMPLATE VectorType;
    typedef MatrixTypeTEMPLATE MatrixType;
    typedef typename VectorType::field_type field_type;
    typedef std::shared_ptr<BasisGridFunction<Basis,VectorType> > GridFunctionPtr;

    NonlinearElasticityProblem(MaterialType& material,
                         const VectorType& extForces):
        material_(Dune::stackobject_to_shared_ptr(material)),
        extForces_(Dune::stackobject_to_shared_ptr(extForces))
    {}

     //! Set external forces
    void setExternalForces(const VectorType& extForces)
    {
        extForces_ = Dune::stackobject_to_shared_ptr(extForces);
    }

    //! Assemble the quadratic problem
    void assembleQP(const VectorType& iterate);

    //! Assemble the preconditioned defect problem
    template <class DiagonalMatrixType>
    void assembleDefectQP(const VectorType& iterate,
                          VectorType& residual,
                          DiagonalMatrixType& hessian);

    //! Compute the energy of the displacement minimization problem
    field_type energy(const VectorType& iterate) const;

    //! Compute the predicted decrease
    field_type modelDecrease(const VectorType& correction) const;

    //! The quadratic part
    MatrixType A_;
    //! The linear functional
    VectorType f_;

private:
    //! The involved materials
    std::shared_ptr<MaterialType> material_;
    //! The external forces
    std::shared_ptr<const VectorType> extForces_;
};

#include "nonlinearelasticityproblem.cc"

#endif
