// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_ELASTICITY_OGDEN_MATERIAL_HH
#define DUNE_ELASTICITY_OGDEN_MATERIAL_HH

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>

#include <dune/fufem/symmetrictensor.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>
#include <dune/fufem/functions/basisgridfunction.hh>
#include <dune/fufem/assemblers/localassemblers/adolclocalenergy.hh>

#include <dune/elasticity/materials/material.hh>
#include <dune/elasticity/assemblers/ogdenfunctionalassembler.hh>
#include <dune/elasticity/assemblers/ogdenoperatorassembler.hh>

/** \brief Class representing Ogden material.
 *
 * \tparam Basis    Global basis that is used for the spatial discretization.
 *
 *    \f[
 *      W(\nabla\varphi)= a tr E(\varphi) + b (tr E(\varphi))^2 + c tr(E(\varphi)^2) + d\Gamma(j)
 *    \f]
 *
 *  where
 *      - \f$E\f$: the nonlinear strain tensor
 *      - \f$tr \f$: the trace operator
 *      - \f$\Gamma(x)=-log(x)\f$: or \f$\Gamma(x)=x^2-log(x)\f$ the penalty function
 *      - \f$J\f$ the determinant of the deformation gradient
 *      - \f$a\f$,\f$b\f$,\f$c\f$,\f$d\f$ material parameters
 *  with
 *       a = -d*Gamma'(1)
 *       b = (\lambda - d*(Gamma'(1)+Gamma''(1)))/2
 *       c = \mu + d*Gamma'(1)
 *       d > 0
 *
 *       such that for small deformations a St.Venant-Kirchhoff-Material is
 *       approximated with Parameters lambda and mu. For d=0, this linear
 *       material law is exactly reproduced.
 */
template <class Basis>
class OgdenMaterial : public Material<Basis>
{
public:
    typedef Material<Basis> Base;
    typedef typename Base::GridType GridType;
    typedef typename Base::GlobalBasis GlobalBasis;
    typedef typename Base::Lfe Lfe;
    typedef typename Base::LocalLinearization LocalLinearization;
    typedef typename Base::LocalHessian LocalHessian;
    typedef typename Base::VectorType VectorType;
    typedef typename Base::GridFunction GridFunction;
    typedef typename Base::ReturnType ReturnType;


private:
    using Base::dim;
    typedef typename GridType::ctype ctype;
    typedef OgdenFunctionalAssembler<GridType,Lfe> OgdenLinearization;
    typedef OgdenOperatorAssembler<GridType, Lfe, Lfe> OgdenHessian;
    typedef typename GridType::template Codim<0>::Geometry::LocalCoordinate LocalCoordinate;

public:
    OgdenMaterial() :
        d_(100)
    {
        ctype lambda(34), mu(136);
        a_= -d_ * Dune::Elasticity::Gamma_x(1);
        b_= (lambda - d_ * (Dune::Elasticity::Gamma_x(1)+Dune::Elasticity::Gamma_xx(1)))/2;
        c_ = mu + d_ * Dune::Elasticity::Gamma_x(1);
    }

    template <class BasisT>
    OgdenMaterial(BasisT&& basis, ctype E, ctype nu, ctype d) :
        Base(std::forward<BasisT>(basis)), d_(d)
    {
        ctype lambda = E*nu / ((1+nu)*(1-2*nu));
        ctype mu = E / (2*(1+nu));
        a_= -d_ * Dune::Elasticity::Gamma_x(1);
        b_= (lambda - d_ * (Dune::Elasticity::Gamma_x(1)+Dune::Elasticity::Gamma_xx(1)))/2;
        c_ = mu + d_ * Dune::Elasticity::Gamma_x(1);


        localLinearization_ = std::shared_ptr<OgdenLinearization>(new OgdenLinearization(lambda,mu,d));
        localHessian_ = std::shared_ptr<OgdenHessian>(new OgdenHessian(lambda,mu,d));
    }

    template <class BasisT>
    void setup(BasisT&& basis, ctype E, ctype nu, ctype d)
    {

        ctype lambda = E*nu / ((1+nu)*(1-2*nu));
        ctype mu = E / (2*(1+nu));

        a_= -d_ * Dune::Elasticity::Gamma_x(1);
        b_= (lambda - d_ * (Dune::Elasticity::Gamma_x(1)+Dune::Elasticity::Gamma_xx(1)))/2;
        c_ = mu + d_ * Dune::Elasticity::Gamma_x(1);

        localLinearization_ = std::shared_ptr<OgdenLinearization>(new OgdenLinearization(lambda,mu,d));
        localHessian_ = std::shared_ptr<OgdenHessian>(new OgdenHessian(lambda,mu,d));

        this->setBasis(std::forward<BasisT>(basis));
    }

    //! Evaluate the strain energy
    ReturnType energy(std::shared_ptr<GridFunction> displace) const
    {
        ReturnType energy=0;
        const auto view = this->basis().getGridView().grid().leafGridView();

        for (const auto& e : elements(view)) {

            // TODO Get proper quadrature rule
            // get quadrature rule
            const int basisOrder = this->basis().getLocalFiniteElement(e).localBasis().order()-1;
            const int order = e.type().isSimplex() ? basisOrder : basisOrder*dim;

            const Dune::template QuadratureRule<ctype, dim>& quad = QuadratureRuleCache<ctype, dim>::rule(e.type(),order,0);

            const auto geometry = e.geometry();

            // loop over quadrature points
            for (size_t pt=0; pt < quad.size(); ++pt) {

                // get quadrature point
                const LocalCoordinate& quadPos = quad[pt].position();

                // get integration factor
                const ctype integrationElement = geometry.integrationElement(quadPos);

                // evaluate displacement gradient at the quadrature point
                typename BasisGridFunction<Basis,VectorType>::DerivativeType localDispGrad;

                if (displace->isDefinedOn(e))
                    displace->evaluateDerivativeLocal(e, quadPos, localDispGrad);
                else
                    displace->evaluateDerivative(geometry.global(quadPos),localDispGrad);

                SymmetricTensor<dim> strain;
                Dune::Elasticity::strain(localDispGrad,strain);

                // the trace
                ReturnType trE = strain.trace();

                Dune::FieldMatrix<ReturnType,dim,dim> EE = strain.matrix();
                EE.leftmultiply(strain.matrix());
                ReturnType trEE = 0;
                for (int i=0; i<dim; i++)
                    trEE += EE[i][i];

                // turn displacement gradient into deformation gradient
                for (int i=0;i<dim;i++)
                    localDispGrad[i][i] += 1;

                // evaluate the derminante of the deformation gradient
                const ReturnType J = localDispGrad.determinant();

                ctype z = quad[pt].weight()*integrationElement;
                energy += z *(a_*trE + b_*trE*trE + c_*trEE + d_*Dune::Elasticity::Gamma(J));
            }
        }

        return energy;
    }

    //! Return the local assembler of the first derivative of the strain energy
    LocalLinearization& firstDerivative(std::shared_ptr<GridFunction> displace) {

        localLinearization_->setConfiguration(displace);
        return *localLinearization_;
    }

    //! Return the local assembler of the second derivative of the strain energy
    LocalHessian& secondDerivative(std::shared_ptr<GridFunction> displace) {

        localHessian_->setConfiguration(displace);
        return *localHessian_;
    }

private:
    //! First derivative of the strain energy
    std::shared_ptr<OgdenLinearization> localLinearization_;

    //! Second derivative of the strain energy
    std::shared_ptr<OgdenHessian> localHessian_;

    //! The Ogden material parameter
    ctype a_;
    //! The Ogden material parameter
    ctype b_;
    //! The Ogden material parameter
    ctype c_;
    //! The Ogden material parameter
    ctype d_;

  };


/** \brief Class reprensenting a local Ogden strain energy functional that can be used within automatic differentiation.
 *
 *    \f[
 *      W(\nabla\varphi)= a tr E(\varphi) + b (tr E(\varphi))^2 + c tr(E(\varphi)^2) + d\Gamma(j)
 *    \f]
 *
 *  where
 *      - \f$E\f$: the nonlinear strain tensor
 *      - \f$tr \f$: the trace operator
 *      - \f$\Gamma(x)=-log(x)\f$: or \f$\Gamma(x)=x^2-log(x)\f$ the penalty function
 *      - \f$J\f$ the determinant of the deformation gradient
 *      - \f$a\f$,\f$b\f$,\f$c\f$,\f$d\f$ material parameters
 *  with
 *       a = -d*Gamma'(1)
 *       b = (\lambda - d*(Gamma'(1)+Gamma''(1)))/2
 *       c = \mu + d*Gamma'(1)
 *       d > 0
 *
 *       such that for small deformations a St.Venant-Kirchhoff-Material is
 *       approximated with Parameters lambda and mu. For d=0, this linear
 *       material law is exactly reproduced.
 */

template <class GridType, class LocalFiniteElement>
class LocalOgdenEnergy : public Adolc::LocalEnergy<GridType, LocalFiniteElement,GridType::dimension>
{
  public:

    enum {dim = GridType::dimension};
    typedef typename GridType::ctype ctype;
    typedef typename GridType::template Codim<0>::Entity Element;

    typedef typename LocalFiniteElement::Traits::LocalBasisType::Traits::RangeFieldType field_type;
    typedef typename LocalFiniteElement::Traits::LocalBasisType::Traits::JacobianType JacobianType;

    typedef Adolc::LocalEnergy<GridType, LocalFiniteElement, dim> Base;
    typedef typename Base::CoefficientVectorType CoefficientVectorType;
    typedef typename Base::ReturnType ReturnType;

    LocalOgdenEnergy(field_type E, field_type nu, field_type d) :
        d_(d)
    {
        field_type lambda = E*nu / ((1+nu)*(1-2*nu));
        field_type mu = E / (2*(1+nu));
        a_= -d_ * Dune::Elasticity::Gamma_x(1);
        b_= (lambda - d_ * (Dune::Elasticity::Gamma_x(1)+Dune::Elasticity::Gamma_xx(1)))/2;
        c_ = mu + d_ * Dune::Elasticity::Gamma_x(1);
    }

    ReturnType energy(const Element& element, const LocalFiniteElement& lfe,
            const CoefficientVectorType& localCoeff) const
    {
        ReturnType energy=0;

        // TODO Get proper quadrature rule
        // get quadrature rule
        const int order = (element.type().isSimplex()) ? 4 : 4*dim;

        const auto& quad = QuadratureRuleCache<ctype, dim>::rule(element.type(),order,
                 IsRefinedLocalFiniteElement<LocalFiniteElement>::value(lfe));

        // the element geometry mapping
        const typename Element::Geometry geometry = element.geometry();

        // store gradients of shape functions and base functions
        std::vector<JacobianType> referenceGradients(lfe.localBasis().size());

        // loop over quadrature points
        for (size_t pt=0; pt < quad.size(); ++pt) {

            // get quadrature point
            const auto& quadPos = quad[pt].position();

            // get integration factor
            const ctype integrationElement = geometry.integrationElement(quadPos);

            // get transposed inverse of Jacobian of transformation
            const auto& invJacobian = geometry.jacobianInverseTransposed(quadPos);

            // get gradients of shape functions
            lfe.localBasis().evaluateJacobian(quadPos, referenceGradients);

            // compute gradient of the configuration
            Dune::FieldMatrix<ReturnType,dim,dim> localGradient(0);
            for (size_t k=0; k<referenceGradients.size(); ++k) {
                Dune::FieldVector<ReturnType,dim> gradient(0);
                invJacobian.umv(referenceGradients[k][0], gradient);
                for (int i=0; i<dim; ++i)
                    for (int j=0; j<dim; ++j)
                        localGradient[i][j] += localCoeff[k][i] * gradient[j];
            }

            SymmetricTensor<dim,ReturnType> strain;
            Dune::Elasticity::strain(localGradient,strain);

            // the trace
            ReturnType trE = strain.trace();

            Dune::FieldMatrix<ReturnType,dim,dim> EE = strain.matrix();
            EE.leftmultiply(strain.matrix());
            ReturnType trEE = 0;
            for (int i=0; i<dim; i++)
                trEE += EE[i][i];

            // turn displacement gradient into deformation gradient
            for (int i=0;i<dim;i++)
                localGradient[i][i] += 1;

            // evaluate the derminante of the deformation gradient
            const ReturnType J = localGradient.determinant();

            ctype z = quad[pt].weight()*integrationElement;
            energy += z *(a_*trE + b_*trE*trE + c_*trEE + d_*Dune::Elasticity::Gamma(J));
        }

        return energy;
    }

  private:
    //! The Ogden material parameter
    ctype a_;
    //! The Ogden material parameter
    ctype b_;
    //! The Ogden material parameter
    ctype c_;
    //! The Ogden material parameter
    ctype d_;
};





#endif
