#ifndef DUNE_ELASTICITY_MATERIALS_SUMENERGY_HH
#define DUNE_ELASTICITY_MATERIALS_SUMENERGY_HH

#include <vector>

#include <dune/elasticity/assemblers/localenergy.hh>

namespace Dune::Elasticity {

template<class LocalView, class field_type=double>
class SumEnergy
  : public Elasticity::LocalEnergy<LocalView, field_type>
{
  using GridView = typename LocalView::GridView;
  using DT = typename GridView::Grid::ctype;

  enum {dim=GridView::dimension};

public:

  /** \brief Constructor with two local energies (compatibility for old interface)*/
  SumEnergy(std::shared_ptr<Elasticity::LocalEnergy<LocalView, field_type>> a,
            std::shared_ptr<Elasticity::LocalEnergy<LocalView, field_type>> b)
  {
    addLocalEnergy(a);
    addLocalEnergy(b);
  }

  /** \brief Default Constructor*/
  SumEnergy()
  {}

  /** \brief Add a local energy to be part of the sum */
  void addLocalEnergy( std::shared_ptr<Elasticity::LocalEnergy<LocalView, field_type>> localEnergy )
  {
    localEnergies_.push_back(localEnergy);
  }

  /** \brief Assemble the energy for a single element */
  field_type energy (const LocalView& localView,
                     const std::vector<field_type>& localConfiguration) const
  {
    field_type sum = 0.;
    for ( const auto& localEnergy : localEnergies_ )
      sum += localEnergy->energy( localView, localConfiguration );

    return sum;
  }

private:
  // vector containing pointers to the local energies
  std::vector<std::shared_ptr<Elasticity::LocalEnergy<LocalView, field_type>>> localEnergies_;
};

}  // namespace Dune::Elasticity


namespace Dune {

template<class GridView, class LocalFiniteElement, class field_type=double>
class
SumEnergy
: public LocalEnergy<GridView,LocalFiniteElement,std::vector<Dune::FieldVector<field_type,GridView::dimension> > >
{
 // grid types
  typedef typename GridView::ctype ctype;
  typedef typename GridView::template Codim<0>::Entity Entity;

  // some other sizes
  enum {dim=GridView::dimension};

public:

  /** \brief Constructor with a set of material parameters
   * \param parameters The material parameters
   */
  SumEnergy(std::shared_ptr<LocalEnergy<GridView,LocalFiniteElement,std::vector<FieldVector<field_type,dim> > > > a,
            std::shared_ptr<LocalEnergy<GridView,LocalFiniteElement,std::vector<FieldVector<field_type,dim> > > > b)
  : a_(a),
    b_(b)
  {}

  /** \brief Assemble the energy for a single element */
  field_type energy (const Entity& element,
                     const LocalFiniteElement& localFiniteElement,
                     const std::vector<Dune::FieldVector<field_type,dim> >& localConfiguration) const
  {
    return a_->energy(element, localFiniteElement, localConfiguration)
         + b_->energy(element, localFiniteElement, localConfiguration);
  }

private:
  [[deprecated("Use dune-functions powerBases with LocalView concept. See Elasticity::SumEnergy")]]
  std::shared_ptr<LocalEnergy<GridView,LocalFiniteElement,std::vector<FieldVector<field_type,dim> > > > a_;

  std::shared_ptr<LocalEnergy<GridView,LocalFiniteElement,std::vector<FieldVector<field_type,dim> > > > b_;

};

}  // namespace Dune


#endif   //#ifndef DUNE_ELASTICITY_MATERIALS_SUMENERGY_HH
