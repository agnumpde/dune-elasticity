#ifndef DUNE_ELASTICITY_MATERIALS_MOONEYRIVLINDENSITY_HH
#define DUNE_ELASTICITY_MATERIALS_MOONEYRIVLINDENSITY_HH

#include <dune/common/fmatrix.hh>

#include <dune/elasticity/materials/localdensity.hh>

namespace Dune::Elasticity {

template<int dim, class field_type = double, class ctype = double>
class MooneyRivlinDensity final
    : public LocalDensity<dim,field_type>
{
public:

  /** \brief Constructor with a set of material parameters
  * \param parameters The material parameters
  */
  MooneyRivlinDensity(const Dune::ParameterTree& parameters)
  {
    mooneyrivlin_energy = parameters.get<std::string>("mooneyrivlin_energy");
    // Mooneyrivlin constants
    if (mooneyrivlin_energy == "ciarlet") {
      mooneyrivlin_a = parameters.get<double>("mooneyrivlin_a");
      mooneyrivlin_b = parameters.get<double>("mooneyrivlin_b");
      mooneyrivlin_c = parameters.get<double>("mooneyrivlin_c");
    }
    else if (mooneyrivlin_energy == "log" or mooneyrivlin_energy == "square")
    {
      mooneyrivlin_10 = parameters.get<double>("mooneyrivlin_10");
      mooneyrivlin_01 = parameters.get<double>("mooneyrivlin_01");
      mooneyrivlin_20 = parameters.get<double>("mooneyrivlin_20");
      mooneyrivlin_02 = parameters.get<double>("mooneyrivlin_02");
      mooneyrivlin_11 = parameters.get<double>("mooneyrivlin_11");
      mooneyrivlin_30 = parameters.get<double>("mooneyrivlin_30");
      mooneyrivlin_03 = parameters.get<double>("mooneyrivlin_03");
      mooneyrivlin_21 = parameters.get<double>("mooneyrivlin_21");
      mooneyrivlin_12 = parameters.get<double>("mooneyrivlin_12");
      mooneyrivlin_k  = parameters.get<double>("mooneyrivlin_k");
    }
    else
    {
      DUNE_THROW(Exception, "Error: Selected mooneyrivlin implementation (" << mooneyrivlin_energy << ") not available!");
    }
  }

  /** \brief Evaluation with the deformation gradient
  *
  * \param x Position of the gradient
  * \param gradient Deformation gradient
  */
  field_type operator() (const FieldVector<ctype,dim>& x, const FieldMatrix<field_type,dim,dim>& gradient) const
  {
    /////////////////////////////////////////////////////////
    // compute C = F^TF
    /////////////////////////////////////////////////////////

    Dune::FieldMatrix<field_type,dim,dim> C(0);
    for (int i=0; i<dim; i++)
      for (int j=0; j<dim; j++)
        for (int k=0; k<dim; k++)
          C[i][j] += gradient[k][i] * gradient[k][j];

    /* The Mooney-Rivlin-Density is given as a function of the eigenvalues of the right Cauchy-Green-Deformation tensor C = F^TF
       or the right Cauchy-Green-Deformation tensor B = FF^T.
       C = F^TF and B = FF^T have the same eigenvalues - we can use either one of them.

       There are three Mooney-Rivlin-Variants:
       ciarlet: W(F) = mooneyrivlin_a*(normF)^2 + mooneyrivlin_b*(normFinv)^2*detF^2 + mooneyrivlin_c*(detF)^2 -
                       (2*mooneyrivlin_a + 4*mooneyrivlin_b + 2*mooneyrivlin_c)*ln(detF),
                       where the last term is chosen s.t. W( t*I ) is minimal for t=1
       log:     W(F) = \sum_{i,j=0}^{i+j<=n} mooneyrivlin_ij * (I1 - 3)^i * (I2 - 3)^j + mooneyrivlin_k * ln(det(F))^2
       square:  W(F) = \sum_{i,j=0}^{i+j<=n} mooneyrivlin_ij * (I1 - 3)^i * (I2 - 3)^j + mooneyrivlin_k * 0.5 * (det(F) - 1)^2

       For log and square: I1 and I2 are the first two invariants of C (or B), multiplied with a factor depending on det(F):
       I1 = (det(F)^(-2/dim)) * [ first invariant of C ]
          = (det(F)^(-2/dim)) * (sum of all eigenvalues of C)
          = (det(F)^(-2/dim)) * trace(C)
          = (det(F)^(-2/dim)) * trace(F^TF)
          = (det(F)^(-2/dim)) * (frobenius_norm(F))^2
       I2 = (det(F)^(-4/dim)) * [ second invariant of C ]
          = (det(F)^(-4/dim)) * 0.5 * [ trace(C)^2 - tr(C^2) ]
          = (det(F)^(-4/dim)) * [C_11C_22 + C_11C_33 + C_22C_33 - C_12C_21 - C_13C_31 - C_23C_32]
          = (det(F)^(-4/dim)) * [C_11C_22 + C_11C_33 + C_22C_33 - C_12^2 - C_13^2 - C_23^2]
    */

    field_type frobeniusNormFsquared = gradient.frobenius_norm2();
    field_type detF = gradient.determinant();

    if (detF < 0)
      DUNE_THROW( Dune::MathError, "det(F) < 0, so it is not possible to calculate the MooneyRivlinEnergy.");

    // Add the local energy density
    field_type strainEnergy = 0;

    if (mooneyrivlin_energy == "ciarlet")
    {
      //To calculate the squared frobeniusnorm of F^(-1), we actually invert F^(-1)
      auto gradientInverse = gradient;
      gradientInverse.invert();
      field_type frobeinusNormFInverseSquared = gradientInverse.frobenius_norm2();
      using std::log;
      return mooneyrivlin_a*frobeniusNormFsquared + mooneyrivlin_b*frobeinusNormFInverseSquared*detF*detF + mooneyrivlin_c*detF*detF
             - (2.0*mooneyrivlin_a + 4.0*mooneyrivlin_b + 2.0*mooneyrivlin_c)*log(detF);
    }
    else
    {
      // mooneyrivlin_energy is "log" or "square"
      field_type a = pow(detF, 2.0/dim);
      field_type invariant1Minus3 = frobeniusNormFsquared/a - 3;
      field_type secondInvariantOfC = 0;
      for (int i=0; i<dim-1; i++)
        for (int j=i+1; j<dim; j++)
          secondInvariantOfC += C[i][i]*C[j][j] - C[i][j]*C[i][j];
      field_type invariant2Minus3 = secondInvariantOfC/(a*a) - 3;
      strainEnergy = mooneyrivlin_10 * invariant1Minus3 +
                     mooneyrivlin_01 * invariant2Minus3 +
                     mooneyrivlin_20 * invariant1Minus3 * invariant1Minus3 +
                     mooneyrivlin_02 * invariant2Minus3 * invariant2Minus3 +
                     mooneyrivlin_11 * invariant1Minus3 * invariant2Minus3 +
                     mooneyrivlin_30 * invariant1Minus3 * invariant1Minus3 * invariant1Minus3 +
                     mooneyrivlin_21 * invariant1Minus3 * invariant1Minus3 * invariant2Minus3 +
                     mooneyrivlin_12 * invariant1Minus3 * invariant2Minus3 * invariant2Minus3 +
                     mooneyrivlin_03 * invariant2Minus3 * invariant2Minus3 * invariant2Minus3;
      if (mooneyrivlin_energy == "log")
      {
        using std::log;
        field_type logDetF = log(detF);
        return strainEnergy + 0.5 * mooneyrivlin_k* logDetF * logDetF;
      }
      else
      {
        //mooneyrivlin_energy is "square"
        field_type detFMinus1 = detF - 1;
        return strainEnergy + 0.5 * mooneyrivlin_k* detFMinus1 * detFMinus1;
      }
    }
  }

  /** \brief Lame constants */
  double mooneyrivlin_a,
         mooneyrivlin_b,
         mooneyrivlin_c,
         mooneyrivlin_10,
         mooneyrivlin_01,
         mooneyrivlin_20,
         mooneyrivlin_02,
         mooneyrivlin_11,
         mooneyrivlin_30,
         mooneyrivlin_21,
         mooneyrivlin_12,
         mooneyrivlin_03,
         mooneyrivlin_k;
  std::string mooneyrivlin_energy;
};

} // namespace Dune::Elasticity

#endif   //#ifndef DUNE_ELASTICITY_MATERIALS_MOONEYRIVLINDENSITY_HH
