#ifndef DUNE_ELASTICITY_MATERIALS_MOONEYRIVLINENERGY_HH
#define DUNE_ELASTICITY_MATERIALS_MOONEYRIVLINENERGY_HH

#include <dune/elasticity/materials/mooneyrivlindensity.hh>
#include <dune/elasticity/materials/localintegralenergy.hh>

namespace Dune {

template<class GridView, class LocalFiniteElement, class field_type=double>
class [[deprecated("Use Elasticity::LocalIntegralEnergy with Elasticity::MooneyRivlinDensity")]]
MooneyRivlinEnergy
  : public Elasticity::LocalIntegralEnergy<GridView,LocalFiniteElement,field_type>
{

  using Base = Elasticity::LocalIntegralEnergy<GridView,LocalFiniteElement,field_type>;
public:

  // backwards compatibility: forward directly to integral energy
  MooneyRivlinEnergy(const Dune::ParameterTree& parameters)
  : Base(std::make_shared<Elasticity::MooneyRivlinDensity<GridView::dimension,field_type>>(parameters))
  {}
};

}  // namespace Dune

#endif   //#ifndef DUNE_ELASTICITY_MATERIALS_MOONEYRIVLINENERGY_HH
