#ifndef DUNE_ELASTICITY_MATERIALS_STVENANTKIRCHHOFFENERGY_HH
#define DUNE_ELASTICITY_MATERIALS_STVENANTKIRCHHOFFENERGY_HH

#include <dune/elasticity/materials/stvenantkirchhoffdensity.hh>
#include <dune/elasticity/materials/localintegralenergy.hh>

namespace Dune {

template<class GridView, class LocalFiniteElement, class field_type=double>
class [[deprecated("Use Elasticity::LocalIntegralEnergy with Elasticity::StVenantKirchhoffDensity")]]
StVenantKirchhoffEnergy
  : public Elasticity::LocalIntegralEnergy<GridView,LocalFiniteElement,field_type>
{

  using Base = Elasticity::LocalIntegralEnergy<GridView,LocalFiniteElement,field_type>;
public:

  // backwards compatibility: forward directly to integral energy
  StVenantKirchhoffEnergy(const Dune::ParameterTree& parameters)
  : Base(std::make_shared<Elasticity::StVenantKirchhoffDensity<GridView::dimension,field_type>>(parameters))
  {}
};

}  // namespace Dune

#endif   //#ifndef DUNE_ELASTICITY_MATERIALS_STVENANTKIRCHHOFFENERGY_HH
