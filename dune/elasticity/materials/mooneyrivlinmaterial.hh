// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_ELASTICITY_MATERIALS_MOONEY_RIVLIN_MATERIAL_HH
#define DUNE_ELASTICITY_MATERIALS_MOONEY_RIVLIN_MATERIAL_HH

#include <dune/fufem/assemblers/localassemblers/adolclocalenergy.hh>

#include <dune/fufem/functions/basisgridfunction.hh>
#include <dune/fufem/quadraturerules/quadraturerulecache.hh>

#include <dune/elasticity/materials/material.hh>
#include <dune/elasticity/common/elasticityhelpers.hh>
#include <dune/elasticity/assemblers/mooneyrivlinfunctionalassembler.hh>
#include <dune/elasticity/assemblers/mooneyrivlinoperatorassembler.hh>

#include <dune/matrix-vector/addtodiagonal.hh>

#warning Mooney-Rivlin Might not work properly yet

/** \brief Class representing a hyperelastic Mooney-Rivlin material.
 *
 * \tparam Basis    Global basis that is used for the spatial discretization.
 *                  (This is not nice but I need a LocalFiniteElement for the local Hessian assembler :-( )
 *
 *    \f[
 *      W(u)= a tr(E(u)) + b tr(E(u))^2 + c ||E(u)||^2 + gamma(J)
 *    \f]
 *
 *  where
 *      - \f$E\f$: the nonlinear strain tensor
 *      - \f$tr \f$: the trace operator
 *      - \f$J\f$ the determinant of the deformation gradient
 *      - \f$a\f$,\f$b\f$,\f$c$\f material parameters
 *      - \f$\gamma$\f A 'penalty' function which enforces orientation preservation
 *
 *  with
 *      \f$\gamma(J)$:= dJ^2 + e J^-k,  k>=2\f
 *      \f$k\f$ controlls orientation preservation (k \approx \floor(1/(1-2nu) -1))
 */
template <class Basis>
class MooneyRivlinMaterial : public Material<Basis>,
            public Adolc::LocalEnergy<typename Material<Basis>::GridType,
                                      typename Material<Basis>::Lfe,
                                      Material<Basis>::GridType::dimension>
{
public:
    typedef Material<Basis> Base;
    using typename Base::GridType;
    using typename Base::GlobalBasis;
    using typename Base::Lfe;
    using typename Base::GridFunction;
    using field_type = typename Base::ReturnType;
    using Base::dim;

    using AdolCBase = Adolc::LocalEnergy<GridType, Lfe, dim>;
    using Element = typename GridType::template Codim<0>::Entity;
    using AdolcCoefficients = typename AdolCBase::CoefficientVectorType;
    using AdolcEnergy = typename AdolCBase::ReturnType;
    using AdolcFieldType = typename AdolCBase::ReturnType;

private:
    using MonRivLinearisation = MooneyRivlinFunctionalAssembler<GridType, Lfe>;
    using MonRivHessian = MooneyRivlinOperatorAssembler<GridType, Lfe, Lfe>;

public:
    MooneyRivlinMaterial() = default;

    template <class BasisT>
    MooneyRivlinMaterial(BasisT&& basis, field_type E, field_type nu, int k=3) :
        Base(std::forward<BasisT>(basis)), E_(E), nu_(nu), k_(k)
    {
        setupCoefficients();
        constructAssemblers();
    }

    template <class BasisT>
    void setup(BasisT&& basis, field_type E, field_type nu, int k=3)
    {
        this->setBasis(std::forward<BasisT>(basis));

        E_ = E; nu_ = nu; k_ = k;
        setupCoefficients();
        constructAssemblers();
    }

    void getMaterialParameters(field_type& E, field_type& nu) {
        E = E_; nu = nu_;
    }

    //! Evaluate the strain energy
    field_type energy(std::shared_ptr<GridFunction> displace) const
    {
        field_type energy=0;
        const auto& leafView = this->basis().getGridView();

        for (const auto& e : elements(leafView)) {

            // TODO Get proper quadrature rule
            // get quadrature rule (should depend on k?)
            const int order = (e.type().isSimplex()) ? 5 : 5*dim;
            const auto& quad = QuadratureRuleCache<field_type, dim>::rule(e.type(), order, 0);

            const auto& geometry = e.geometry();

            // loop over quadrature points
            for (const auto& pt : quad) {

                const auto& quadPos = pt.position();
                auto integrationElement = geometry.integrationElement(quadPos);

                // evaluate displacement gradient at the quadrature point
                typename BasisGridFunction<Basis, typename Base::VectorType>::DerivativeType localConfGrad;

                if (displace->isDefinedOn(e))
                    displace->evaluateDerivativeLocal(e, quadPos, localConfGrad);
                else
                    displace->evaluateDerivative(geometry.global(quadPos), localConfGrad);

                auto strain = Dune::Elasticity::strain(localConfGrad);
                auto trE = strain.trace();

                // make deformation gradient out of the discplacement
                Dune::MatrixVector::addToDiagonal(localConfGrad, 1.0);

                auto J = localConfGrad.determinant();

                auto z = pt.weight()*integrationElement;

                // W(u)= a tr(E(u)) + b tr(E(u))^2 + c ||E(u)||^2 + gamma(J)
                energy += z*(a_*trE + b_*trE*trE + c_*(strain*strain) + d_*J*J + e_*std::pow(J,-k_));
            }
        }
        return energy;
    }

    //! Compute local energy, required for the Adol-C interface
    AdolcEnergy energy(const Element& element, const Lfe& lfe,
            const AdolcCoefficients& localCoeff) const
    {
        AdolcEnergy energy=0;

        // TODO Get proper quadrature rule
        // get quadrature rule (should depend on k?)
        const int order = (element.type().isSimplex()) ? 5 : 5*dim;
        const auto& quad = QuadratureRuleCache<typename GridType::ctype, dim>::rule(element.type(), order, 0);

        const auto& geometry = element.geometry();

        using LfeTraits = typename Lfe::Traits::LocalBasisType::Traits;
        using Jacobian = typename LfeTraits::JacobianType;
        std::vector<Jacobian> referenceGradients(lfe.localBasis().size());

        for (const auto& pt : quad) {

            const auto& quadPos = pt.position();
            auto integrationElement = geometry.integrationElement(quadPos);
            const auto& invJacobian = geometry.jacobianInverseTransposed(quadPos);


            // get gradients of shape functions
            lfe.localBasis().evaluateJacobian(quadPos, referenceGradients);

            // compute gradient of the configuration
            Dune::FieldMatrix<AdolcFieldType, dim, dim> localGradient(0);
            for (size_t k=0; k < referenceGradients.size(); ++k) {

              Dune::FieldVector<AdolcFieldType, dim> gradient(0);
              invJacobian.umv(referenceGradients[k][0], gradient);

              for (int i=0; i < dim; ++i)
                for (int j=0; j < dim; ++j)
                  localGradient[i][j] += localCoeff[k][i] * gradient[j];
            }

            auto strain = Dune::Elasticity::strain(localGradient);
            auto trE = strain.trace();

            // make deformation gradient out of the discplacement
            Dune::MatrixVector::addToDiagonal(localGradient, 1.0);

            auto J = localGradient.determinant();

            auto z = pt.weight()*integrationElement;

            // W(u)= a tr(E(u)) + b tr(E(u))^2 + c ||E(u)||^2 + gamma(J)
            energy += z*(a_*trE + b_*trE*trE + c_*(strain*strain) + d_*J*J + e_*std::pow(J,-k_));
        }
        return energy;
    }

    //! Return the local assembler of the first derivative of the strain energy
    typename Base::LocalLinearization& firstDerivative(std::shared_ptr<GridFunction> displace) {
        localLinearisation_.setConfiguration(displace);
        return localLinearisation_;
    }

    //! Return the local assembler of the second derivative of the strain energy
    typename Base::LocalHessian& secondDerivative(std::shared_ptr<GridFunction> displace) {
        localHessian_.setConfiguration(displace);
        return localHessian_;
    }

private:
    //! Compute coefficients s.t. polyconvexity holds
    void setupCoefficients() {

        field_type lambda = E_*nu_ / ((1 +nu_)*(1 - 2*nu_));
        field_type mu     = E_ / (2*(1 + nu_));

        // Choose coefficients in such a way that we have polyconvexity
        // First/Second derivative of the compressible function part of gamma (=-kJ^(-k-1)) at 1.0
        field_type ld1 = -k_;
        field_type ld2 = k_*(k_+1);
        if (ld1 >=0 || ld2 <= 0)
            std::cout<<"Coerciveness failed\n";

        // Check if lame constants admit coerciveness
        field_type rho= -ld1/(ld2-ld1);

        if( ( (rho < 0.5 && lambda < (1.0/rho-2.0)*mu) || lambda <= 0 || mu <= 0 ))
            std::cout<<"Coerciveness failed\n";

        //const field_type somePositiveConstant = - mu_ + rho*(lambda_+2.*mu_);
        //if(somePositiveConstant <= 0)

        e_ = (lambda+2.0*mu)/(ld2-ld1);
        if(e_ <= 0 )
            std::cout<<"Coerciveness failed\n";

        // parameters a, and b are used if expressed in terms of the Green-StVenant strain tensor
        // the choice of b and d is admissible but heuristic -> @TODO justified choice
        d_ = (0.5*rho-0.25)*mu+0.25*rho*lambda;
        if(d_ > 0.25*mu)
            d_ = (rho-0.75)*mu+0.5*rho*lambda;

        b_ = -mu + rho*(lambda+2.*mu)-2.*d_;
        c_ = -b_;
        a_ = b_ + mu;

        // last check if I didn't miss a condition
        field_type alpha = 0.5*(mu - b_);
        if(alpha <= 0 || b_ <= 0 || d_ <= 0)
            std::cout<<"Coerciveness failed\n";
    }

    //! Construct the local assembler members
    void constructAssemblers() {
        localLinearisation_ = MonRivLinearisation(a_, b_, c_, d_, e_, k_);
        localHessian_ = MonRivHessian(a_, b_, c_, d_, e_, k_);
    }

    //! First derivative of the strain energy
    MonRivLinearisation localLinearisation_;

    //! Second derivative of the strain energy
    MonRivHessian localHessian_;

    //! Elasticity modulus
    field_type E_;
    //! Shear modulus
    field_type nu_;
    //! Exponent of the compressibility function (>= 2)
    int k_;
    //! Material parameters
    field_type a_; field_type b_; field_type c_; field_type d_; field_type e_;
};

#endif
